//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
  //port = process.env.PORT || 3000;

var path = require('path');

var requestjson = require('request-json');
var urlMlabRaiz ="https://api.mlab.com/api/1/databases/jgarza2/collections"
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
var clienteMlabRaiz;

var urlClientes = "https://api.mlab.com/api/1/databases/jgarza2/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var urlProductos ="https://api.mlab.com/api/1/databases/jgarza2/collections/Productos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLab = requestjson.createClient(urlClientes)

app.listen(port);

var bodyparser = require('body-parser')
app.use(bodyparser.json())
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()

})

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname,'index.html'));

})

app.get('/Clientes', function(req, res) {
  clienteMLab.get('', function(err, resM, body){
    if (err){
      console.log(body)
    }else{
      res.send(body);
    }
  })
})

app.get('/Productos', function(req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type")
  var urlMLab = urlMlabRaiz + "/Productos?" + "&" + apiKey;
  console.log(urlMLab)
  productosMlabRaiz = requestjson.createClient(urlMLab)
  productosMlabRaiz.get('', function(err, resM, body) {
    if (err) {
      res.status(400).send('Productos no encontrados')
    }else{
      res.status(200).send(body)
    }
  })
})

app.get('/Proveedores', function(req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type")
  var urlMLab = urlMlabRaiz + "/Proveedores?" + "&" + apiKey;
  console.log(urlMLab)
  productosMlabRaiz = requestjson.createClient(urlMLab)
  productosMlabRaiz.get('', function(err, resM, body) {
    if (err) {
      res.status(400).send('Proveedores no encontrados')
    }else{
      res.status(200).send(body)
    }
  })
})

app.post('/altaProductos', function(req, res) {
  var urlMLab = urlMlabRaiz + "/Productos?" + "&" + apiKey;
  console.log(urlMLab)
  altaProductosMlabRaiz = requestjson.createClient(urlMLab)
  altaProductosMlabRaiz.post('', req.body, function(err, resM, body) {
    if (err) {
      res.status(400).send('Ocurrio un error al realizar la alta del producto')
    }else{
      res.status(200).send(body)
    }
  })
})

app.post('/Clientes', function(req, res){
  clienteMLab.post('', req.body, function(err, resM, body){
    res.send(body)
  })
})

app.post('/Login', function(req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type")
  var email = req.body.email
  var password = req.body.password
  var query = 'q={"email":"'+ email +'","password":"' +password + '"}'
  console.log(query)
  var urlMLab = urlMlabRaiz + "/Usuarios?" + query + "&" + apiKey;
  console.log(urlMLab)
  clienteMlabRaiz = requestjson.createClient(urlMLab)
  clienteMlabRaiz.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1) { //Login ok, se encontro 1 documento
        res.status(200).send('Usuario logado')
      } else { //No se encontro al usuario
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})
